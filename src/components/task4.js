class Square {
  constructor(side) {
    this._side = side;
  }

  side() {
    return this._side;
  }

  perimeter() {
    return 4 * this._side;
  }

}

export class CubeWithParentMethod extends Square {
  constructor(side) {
    super(side);

    this._side = side;
  }

  perimeter() {
    const sPerimeter = super.perimeter();
    return 8 * this._side + sPerimeter;
  }
}

export class CubeWithoutParentMethod extends Square {
  constructor(side) {
    super(side);

    this._side = side;
  }

  perimeter() {
    return 12 * this._side
  }
}
