

class GeometricFigure {
  constructor(width, height) {
    this._width = width;
    this._height = height;
  }

  width() {
    return this._width;
  }

  height() {
    return this._height;
  }
}

export class Rectangle extends GeometricFigure {
  constructor(width, height, diagonal) {
    super(width,height)

    this._diagonal = diagonal;
  }

  figureCenter() {
    return `[${this.width() / 2}, ${this.height() / 2}] `
  }

  info() {
    return this;
  }
}

export class Circle extends GeometricFigure {
  constructor(width, height, radius) {
    super(width, height)

    this._radius = radius;
  }

  figureCenter() {
    return `[${this.width() / 2}, ${this.height() / 2}] `
  }

  info() {
    return this;
  }
}