import React from 'react';

import Select from './components/Select';
import { Rectangle, Circle } from './components/task1';
import { CubeWithoutParentMethod, CubeWithParentMethod } from './components/task4';

const languages = ["UK","JP","US","PL","DE","FR","DA","KA"];

// API URI should be '/api/translate', to check if my posts work i added a mocked API URI
const URI = "https://demo2295027.mockable.io/get/translate"

class App extends React.Component  {
  constructor() {
    super();

    this.state = {
      from: '',
      to: '',
      text: '',
      translatedText: '',
      promiseResult1: '',
      promiseResult2: '',
    }

    //task 1 instances
    this.Task1_Rectangle =  new Rectangle(50,50, 70.711);
    this.Task1_Circle =  new Circle(20,20, 10);

    //task 4 instances
    this.Task4_CubeWithParentMethod = new CubeWithParentMethod(30);
    this.Task4_CubeWithoutParentMethod = new CubeWithoutParentMethod(30);
  };

  handleChange = (e) => {
    this.setState({ [e.target.name] : e.target.value });
  };

  fetchTranslatedWord = async (body) => {
    try {
      const response = await fetch(URI, {
        method: 'POST',
        body,
      })

      const { text } = await response.json();

      this.setState({
        translatedText: text,
      })
    } catch(error) {
      console.log(error);
    }
  }

  handleTranslate = async () => {
    const { from, to, text } = this.state;
    await this.fetchTranslatedWord({
      from,
      to,
      text,
    });
  }

  handleTestPromise = () => {
    let p1 = this.createPromiseRandom(1000,2000, 2000);
    p1.then((data) => this.setState({ promiseResult1: data }))
      .catch((error) => this.setState({ promiseResult1: error}));

    let p2 = this.createPromiseRandom(3000,2000, 3000);
    p2.then((data) => this.setState({ promiseResult2: data }))
      .catch((error) => this.setState({ promiseResult2: error}));
  }

  createPromiseRandom(min,max,delay) {
    return new Promise((resolve, reject) => {
        setTimeout(function() {
          if (min < max) {
            resolve(Math.random() * (max - min) + min);
          } else {
            reject("This promise couldn't be resolved");
          }
        }, delay);
    })
  }

  handleTask1 = () => {
    console.log('// TASK 1')

    console.log('Reactangle width: ',this.Task1_Rectangle.width());
    console.log('Reactangle height: ',this.Task1_Rectangle.height());
    console.log('Reactangle info: ',this.Task1_Rectangle.info());
    console.log('Reactangle figureCenter: ',this.Task1_Rectangle.figureCenter());

    console.log('Circle width: ',this.Task1_Circle.width());
    console.log('Circle height: ',this.Task1_Circle.height());
    console.log('Circle info: ',this.Task1_Circle.info());
    console.log('Circle figureCenter: ',this.Task1_Circle.figureCenter());
  }
  handleTask4 = () => {
    console.log('// TASK 4')

    console.log('Cube1 width: ',this.Task4_CubeWithParentMethod.width());
    console.log('Cube2 width: ',this.Task4_CubeWithoutParentMethod.width());
    console.log('Cube1 perimeter: ',this.Task4_CubeWithParentMethod.perimeter());
    console.log('Cube2 perimeter: ',this.Task4_CubeWithoutParentMethod.perimeter());

  }


  render() {
    const { from, to, text, translatedText, promiseResult1, promiseResult2 } = this.state;
    const ContainerStyle = {
      margin: '5rem',
      display: 'flex',
      flexDirection: 'column',
      justifyContent: 'space-between',
    }
    return (
      <div style={ContainerStyle}>
        <div>
          <h1>TASK 1</h1>
          <p>Rectangle:</p>
          <ul>
            <li>width: 50</li>
            <li>height: 50</li>
            <li>diagonal: 70.711</li>
          </ul>
          <p>Circle:</p>
          <ul>
            <li>width: 20</li>
            <li>height: 20</li>
            <li>radius: 10</li>
          </ul>
          <p>After clicking button check console</p>
          <button onClick={this.handleTask1}>CONSOLE LOG</button>
        </div>
        <div>
          <h1>TASK 2</h1>
          <Select handleChange={this.handleChange} value={from} list={languages} name="from"/>
          <button onClick={this.handleTranslate}>translate</button>
          <Select handleChange={this.handleChange} value={to} list={languages} name="to"/>
          <div className="ContentArea">
            <textarea onChange={this.handleChange} name="text" value={text}/>
            <textarea value={translatedText}/>
          </div>
        </div>

        <div>
          <h1>TASK 3</h1>
          <button onClick={this.handleTestPromise}>Test Promise</button>
          <h2>Resolve</h2>
          <p>{promiseResult1}</p>
          <h2>Reject</h2>
          <p>{promiseResult2}</p>
        </div>

        <div>
          <h1>TASK 4</h1>
          <p>Rectangle:</p>
          <ul>
            <li>width: 50</li>
            <li>height: 50</li>
            <li>diagonal: 70.711</li>
          </ul>
          <p>After clicking button check console</p>
          <button onClick={this.handleTask4}>CONSOLE LOG</button>
        </div>
      </div>
    )
  }
}

export default App;

