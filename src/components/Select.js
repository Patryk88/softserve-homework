import React from 'react';

const Select = ({ list, value, handleChange, name }) => (
  <select onChange={handleChange} value={value} name={name}>
    {
      list.map((country, index) => {
        return <option key={index} value={country}>{country}</option>
      })
    }
  </select>
);

export default Select;
